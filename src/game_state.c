
#include "game_state.h"

GameState*
allocGameState(void)
{
  GameState* gameState;

  gameState = SDL_malloc(sizeof(GameState));
  SDL_assert(gameState != NULL);
  SDL_memset(gameState, 0, sizeof(GameState));

  return gameState;
}

void
freeGameState(GameState* gameState)
{
  SDL_free(gameState);
}
