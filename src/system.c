#include "system.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

SDL_Window* window;
SDL_Renderer* renderer;

_Noreturn void
systemExit(void)
{
#ifndef DEBUG
  ((char**)NULL)[0] = "Segmentation fault";
#endif
  exit(0);
}

void
systemInit(void)
{
  SDL_assert(SDL_Init(SDL_INIT_EVERYTHING) == 0);
  SDL_assert(IMG_Init(IMG_INIT_PNG) != 0);

  window = SDL_CreateWindow("i wanna see the segmentation fault!",
                            SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED,
                            640, 480,
                            SDL_WINDOW_RESIZABLE);
  SDL_assert(window != NULL);
  
  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
  SDL_assert(renderer != NULL);

  return;
}

void
systemDeinit(void)
{
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  IMG_Quit();
  SDL_Quit();

  return;
}
