
#include "scene.h"
#include "texture.h"

SDL_bool
sceneCollideDot(SDL_FPoint point, Scene* scene)
{
  for (Uint16 i = 0; i < scene->meshesLen; ++i)
    if (meshCollideDot(scene->meshes[i], point))
      return SDL_TRUE;
  return SDL_FALSE;
}

SDL_bool
sceneRaycast(SDL_FPoint start, SDL_FPoint dir, Uint16 count, Scene* scene)
{
  for (Uint16 i = 0; i < count; ++i)
    {
      if (sceneCollideDot(start, scene))
        return SDL_TRUE;
      start.x += dir.x;
      start.y += dir.y;
    }
  return SDL_FALSE;
}

RenderMesh*
sceneColliding(SDL_FRect rect, Scene* scene)
{
  for (Uint16 i = 0; i < scene->meshesLen; ++i)
    if (meshCollideRect(scene->meshes[i], rect))
      return scene->meshes[i];
  return NULL;
}

Scene*
sceneCreate(Uint16 size)
{
  Scene* scene;

  scene = SDL_malloc(sizeof(Scene));
  SDL_assert(scene);

  scene->meshesMaxSize = size;
  scene->meshesLen = 0;
  scene->backgroundTexture = NULL;
  scene->meshes = SDL_malloc(sizeof(RenderMesh*) * size);
  SDL_assert(scene->meshes);
  SDL_memset(scene->meshes, 0, sizeof(RenderMesh*) * size);

  return scene;
}

void
sceneFree(Scene* scene)
{
  for (Uint16 i = 0; i < scene->meshesLen; ++i)
    renderMeshFree(scene->meshes[i]);
  if (scene->backgroundTexture != NULL)
    SDL_DestroyTexture(scene->backgroundTexture);
  free(scene);
}

void
sceneAddMesh(Scene* scene, RenderMesh* mesh)
{
  scene->meshes[scene->meshesLen++] = mesh;
}

void
sceneDraw(SDL_Renderer* renderer, Scene* scene)
{
  if (scene->backgroundTexture != NULL)
    SDL_RenderCopy(renderer, scene->backgroundTexture, NULL, NULL);
  for (Uint16 i = 0; i < scene->meshesLen; ++i)
    renderMeshDraw(renderer, scene->meshes[i]);
}

SDL_FRect*
sceneGetFRects(Scene* scene, Arena* arena)
{
  SDL_FRect* result;
  result = (void*)((Uint64)arena->start + arena->position);
  for (Uint16 i = 0; i < scene->meshesLen; ++i)
    {
      SDL_FRect* rect;
      rect = arenaPush(arena, sizeof(SDL_FRect));
      if (rect == NULL)
        {
          SDL_Log("error, can't push rect into arena alloc\n");
          return NULL;
        }
      memcpy(rect, &(scene->meshes[i]->rect), sizeof(SDL_FRect));
    }
  return result;
}

void
sceneLoad(Scene* scene, SDL_Renderer* renderer, const char* path)
{
  SDL_RWops* fdSc;
  char buffer[0x200];
  char* current_line;
  float x, y, w, h, r, g, b;
  char texturePath[0x100];
  SDL_Texture* texture;
  Uint32 offset;

  fdSc = SDL_RWFromFile(path, "r");
  SDL_assert(fdSc != NULL);

  SDL_RWread(fdSc, buffer, sizeof(buffer), sizeof(char));
  current_line = buffer;

  while (current_line[0] != '\0')
    {
      SDL_sscanf(current_line, "%f %f %f %f %f %f %f %s\n%n", &x, &y, &w, &h, &r, &g, &b, texturePath, &offset);

      if (SDL_strcmp(texturePath, "NULL") != 0)
        texture = textureLoad(renderer, texturePath);
      else
        texture = NULL;
      sceneAddMesh(scene,
                   renderMeshCreate((SDL_FRect) {x, y, w, h},
                                    (SDL_Color) {r, g, b},
                                    (SDL_Texture*) texture));
      current_line += offset;
    }

  SDL_RWclose(fdSc);
}
