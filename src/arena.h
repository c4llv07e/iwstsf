
#ifndef IWS_ARENA_INC
#define IWS_ARENA_INC

#include <SDL2/SDL.h>

typedef struct Arena Arena;
struct Arena
{
  void* start;
  Uint32 position;
  Uint32 size;
};

Arena arenaCreate(Uint32 size);
void arenaRelease(Arena* arena);

void* arenaPush(Arena* arena, Uint32 size);
void arenaPop(Arena* arena, Uint32 size);

Uint32 arenaGetPos(Arena* arena);
void arenaSetPos(Arena* arena, Uint32 pos);
void arenaReset(Arena* arena);

#endif
