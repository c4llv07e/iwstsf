
#ifndef IWS_DEBUG_INC
#define IWS_DEBUG_INC

#include <SDL2/SDL.h>

void applyDebugColor(SDL_Renderer* renderer);
void applyDebugSelectedColor(SDL_Renderer* renderer);
void debugDrawRectF(SDL_Renderer* renderer, SDL_FRect rect);
void debugDrawSelectedRectF(SDL_Renderer* renderer, SDL_FRect rect);
void debugDrawLineF(SDL_Renderer* renderer, SDL_FPoint start, SDL_FPoint end);
void debugDrawVector(SDL_Renderer* renderer, SDL_FPoint start, SDL_FPoint vec);
void debugDrawVectorResize(SDL_Renderer* renderer, SDL_FPoint start,
                           SDL_FPoint vec, double size);

#endif
