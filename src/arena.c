
#include "arena.h"

Arena
arenaCreate(Uint32 size)
{
  Arena arena;

  arena.start = SDL_malloc(size);
  SDL_assert(arena.start != NULL);
  arena.position = 0;
  arena.size = size;

  return arena;
}

void
arenaRelease(Arena* arena)
{
  SDL_free(arena->start);
}

void*
arenaPush(Arena* arena, Uint32 size)
{
  void* result;
  if (arena->position + size > arena->size)
    return NULL;
  
  result = (void*)((Uint64)arena->start + arena->position);
  arena->position += size;
  return result;
}

void
arenaPop(Arena* arena, Uint32 size)
{
  arena->position -= size;
}

Uint32
arenaGetPos(Arena* arena)
{
  return arena->position;
}

void
arenaSetPos(Arena* arena, Uint32 pos)
{
  arena->position = pos;
}

void
arenaReset(Arena* arena)
{
  arena->position = 0;
}
