
#include "render_mesh.h"
#include "utils.h"

RenderMesh*
renderMeshCreate(SDL_FRect rect, SDL_Color color, SDL_Texture* texture)
{
  RenderMesh* mesh;

  mesh = SDL_malloc(sizeof(RenderMesh));
  SDL_assert(mesh);

  mesh->rect = rect;
  mesh->color = color;
  mesh->texture = texture;

  return mesh;
}

void
renderMeshFree(RenderMesh* mesh)
{
  if (mesh->texture != NULL)
    SDL_DestroyTexture(mesh->texture);
  SDL_free(mesh);
}

void
renderMeshDraw(SDL_Renderer* renderer, RenderMesh* mesh)
{
  if (mesh->texture != NULL)
    {
      SDL_Rect pos;
      pos = frectToRect(mesh->rect);
      SDL_RenderCopy(renderer, mesh->texture, NULL, &(pos));
    }
  else
    {
      setColor(renderer, mesh->color);
      SDL_RenderFillRectF(renderer, &mesh->rect);
    }
}

SDL_bool
meshCollideDot(RenderMesh* mesh, SDL_FPoint dot)
{
  return dot.x >= mesh->rect.x && dot.x <= mesh->rect.x + mesh->rect.w &&
    dot.y >= mesh->rect.y && dot.y <= mesh->rect.y + mesh->rect.h;
}

SDL_bool
meshCollideRect(RenderMesh* mesh, SDL_FRect b)
{
  const SDL_FRect a = mesh->rect;
  return ((a.x <= b.x + b.w) &&
          (a.x + a.w >= b.x) &&
          (a.y <= b.y + b.h) &&
          (a.y + a.h >= b.y));
}

Uint8
getMeshCollisionRect(RenderMesh* mesh, SDL_FRect a)
{
  const SDL_FRect b = mesh->rect;
  const SDL_bool isHorizontal = ((a.y < b.y && a.y + a.h+3 > b.y + b.h) || (a.y > b.y && a.y+3 < b.y + b.h) || (a.y + a.h - 3 > b.y && a.y + a.h < b.y + b.h));
  const SDL_bool isVertical = ((a.x < b.x && a.x + a.w+5 > b.x + b.w) || (a.x > b.x && a.x+5 < b.x + b.w) || (a.x + a.w - 5 > b.x && a.x + a.w < b.x + b.w));
  const SDL_bool aRightB = (a.x < b.x) && (a.x + a.w > b.x) && isHorizontal;
  const SDL_bool aLeftB  = (a.x + a.w > b.x + b.w) && (a.x < b.x + b.w) && isHorizontal;
  const SDL_bool aBelowB = (a.y < b.y + b.h) && (a.y + a.h > b.y + b.h) && isVertical;
  const SDL_bool aAboveB = (a.y + a.h > b.y) && (a.y < b.y) && isVertical;
  return ((aRightB & 1)) +
    ((aLeftB & 1) << 1) +
    ((aBelowB & 1) << 2) +
    ((aAboveB & 1) << 3);
}
