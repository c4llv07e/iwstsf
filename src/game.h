
#ifndef IWS_GAME_INC
#define IWS_GAME_INC

#include "game_state.h"

extern GameState* mainGame;

void gameInit(void);
void gameDeinit(void);
void gameRender(void);
void gameUpdate(void);

#endif
