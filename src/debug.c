
#include "debug.h"

void
applyDebugColor(SDL_Renderer* renderer)
{
  SDL_SetRenderDrawColor(renderer, 0x00, 0xff, 0xff, 0xff);
}

void
applyDebugSelectedColor(SDL_Renderer* renderer)
{
  SDL_SetRenderDrawColor(renderer, 0x00, 0xff, 0x00, 0xff);
}

void
debugDrawRectF(SDL_Renderer* renderer, SDL_FRect rect)
{
  SDL_RenderDrawRectF(renderer, &rect);
}

void
debugDrawSelectedRectF(SDL_Renderer* renderer, SDL_FRect rect)
{
  SDL_RenderDrawRectF(renderer, &rect);
}
  
void
debugDrawLineF(SDL_Renderer* renderer, SDL_FPoint start, SDL_FPoint end)
{
  SDL_RenderDrawLineF(renderer, start.x, start.y, end.x, end.y);
}

void
debugDrawVector(SDL_Renderer* renderer, SDL_FPoint start, SDL_FPoint vec)
{
  SDL_RenderDrawLineF(renderer, start.x, start.y,
                      start.x + vec.x,
                      start.y + vec.y);
}
void
debugDrawVectorResize(SDL_Renderer* renderer, SDL_FPoint start,
                      SDL_FPoint vec, double size)
{
  SDL_RenderDrawLineF(renderer, start.x, start.y,
                      start.x + vec.x * size,
                      start.y + vec.y * size);
}
