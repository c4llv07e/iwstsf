
#ifndef IWS_SCENE_INC
#define IWS_SCENE_INC

#include <SDL2/SDL.h>
#include "render_mesh.h"
#include "arena.h"

typedef struct Scene
{
  Uint16 meshesMaxSize;
  Uint16 meshesLen;
  RenderMesh** meshes;
  SDL_Texture* backgroundTexture;
} Scene;

SDL_bool sceneCollideDot(SDL_FPoint point, Scene* scene);
SDL_bool sceneRaycast(SDL_FPoint start, SDL_FPoint dir, Uint16 count, Scene* scene);
RenderMesh* sceneColliding(SDL_FRect rect, Scene* scene);
Scene* sceneCreate(Uint16 size);
void sceneFree(Scene* scene);
void sceneAddMesh(Scene* scene, RenderMesh* mesh);
void sceneDraw(SDL_Renderer* renderer, Scene* scene);
SDL_FRect* sceneGetFRects(Scene* scene, Arena* arena);
void sceneLoad(Scene* scene, SDL_Renderer* renderer, const char* path);

#endif
