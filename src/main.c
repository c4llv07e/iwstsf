
#include "system.h"
#include "game.h"
#include "delta_time.h"

int
main(void)
{
  systemInit();
  gameInit();

  deltaAfterFrame();
  while (mainGame->isGameRunning)
    {
      deltaBeforeFrame();

      gameUpdate();
      gameRender();
      deltaAfterFrame();
    }

  gameDeinit();
  systemDeinit();
  systemExit();
  return 0;
}
