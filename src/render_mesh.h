
#ifndef IWS_RENDER_MESH_INC
#define IWS_RENDER_MESH_INC

#include <SDL2/SDL.h>

typedef struct RenderMesh
{
  SDL_FRect rect;
  SDL_Color color;
  SDL_Texture* texture;
} RenderMesh;

RenderMesh* renderMeshCreate(SDL_FRect rect, SDL_Color color, SDL_Texture* texture);
void renderMeshFree(RenderMesh* mesh);
void renderMeshDraw(SDL_Renderer* renderer, RenderMesh* mesh);
SDL_bool meshCollideDot(RenderMesh* mesh, SDL_FPoint dot);
SDL_bool meshCollideRect(RenderMesh* mesh, SDL_FRect rect);
Uint8 getMeshCollisionRect(RenderMesh* mesh, SDL_FRect b);

#endif
