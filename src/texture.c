
#include "texture.h"

SDL_Texture*
textureLoad(SDL_Renderer* renderer, const char* path)
{
  SDL_Surface* surface;
  SDL_Texture* texture;
  
  surface = IMG_Load(path);
  SDL_assert(surface);

  texture = SDL_CreateTextureFromSurface(renderer, surface);
  SDL_assert(texture);
  
  SDL_FreeSurface(surface);
  return texture;
}
