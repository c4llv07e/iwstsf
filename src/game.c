
#include <SDL2/SDL.h>
#include "game.h"
#include "system.h"
#include "events.h"
#include "delta_time.h"
#include "debug.h"
#include "utils.h"
#include "game_state.h"
#include "arena.h"
#include "texture.h"

GameState* mainGame;
Player* player;

Player*
createPlayer(void)
{
  Player* player;

  player = SDL_malloc(sizeof(Player));
  SDL_assert(player != NULL);
  SDL_memset(player, 0, sizeof(Player));

  player->rect = (SDL_FRect) { 100.0f, 360.0f, 30.0f, 30.0f };
  player->vel = (SDL_FPoint) { 0.0f, 0.0f };
  player->speed = 0.2f;
  player->isOnFloor = SDL_FALSE;
  player->jumpHeight = 5.0f;

  return player;
}

void
destroyPlayer(Player* player)
{
  SDL_free(player);
}

void
gameInit(void)
{
  mainGame = allocGameState();
  mainGame->isGameRunning = SDL_TRUE;
  mainGame->scene = sceneCreate(25);
  sceneLoad(mainGame->scene, renderer, "test.scene");
  mainGame->sceneRectsArena = arenaCreate(sizeof(SDL_FRect) * mainGame->scene->meshesMaxSize);
  mainGame->sceneRects = sceneGetFRects(mainGame->scene, &(mainGame->sceneRectsArena));
  mainGame->gravity = 0.02f;

  mainGame->player = createPlayer();
  player = mainGame->player;
}

void
gameDeinit(void)
{
  sceneFree(mainGame->scene);
  arenaRelease(&(mainGame->sceneRectsArena));
  destroyPlayer(mainGame->player);
  freeGameState(mainGame);
}

void
gameUpdate(void)
{
  float wishDir;
  SDL_bool isJumping;
  SDL_FPoint moveResult;
  
  pollEvents(mainGame);

  wishDir = ((int)keysDown[SDL_SCANCODE_RIGHT] - (int)keysDown[SDL_SCANCODE_LEFT]);
  isJumping = keysDown[SDL_SCANCODE_Z];

  player->vel.x = wishDir * deltaTime * player->speed;
  player->vel.y += deltaTime * mainGame->gravity;

  if (player->isOnFloor)
    player->jumpsDone = 0;

  if (isJumping && player->jumpsDone < 2 && !player->lastJump)
    {
      player->vel.y = -player->jumpHeight;
      player->jumpsDone += 1;
    }

  player->lastJump = isJumping;

  moveResult = moveFrect(player->rect, &(player->vel),
                         mainGame->sceneRects, mainGame->scene->meshesLen);
  
  player->isOnFloor = moveResult.y < 0.0f;

  player->rect.x += player->vel.x;
  player->rect.y += player->vel.y;
}

void
gameRender(void)
{
  SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xff);
  SDL_RenderClear(renderer);

  sceneDraw(renderer, mainGame->scene);

  applyDebugColor(renderer);
  debugDrawRectF(renderer, player->rect);
  
  for (Uint32 i = 0; i < mainGame->scene->meshesLen; ++i)
    {
      renderMeshDraw(renderer, mainGame->scene->meshes[i]);
    }

  SDL_RenderPresent(renderer);
  SDL_Delay(SDL_max(8 - deltaTime, 8));
}
