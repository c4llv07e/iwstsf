
#include "events.h"
#include "game.h"

SDL_bool keysDown[SDL_NUM_SCANCODES] = {SDL_FALSE};
SDL_Point mousePosition;

void
pollEvents(GameState* state)
{
  SDL_Event event;
  while (SDL_PollEvent(&event))
    {
      switch (event.type)
        {
        case SDL_QUIT:
          state->isGameRunning = SDL_FALSE;
          break;
        case SDL_KEYDOWN:
          keysDown[event.key.keysym.scancode] = SDL_TRUE;
          break;
        case SDL_KEYUP:
          keysDown[event.key.keysym.scancode] = SDL_FALSE;
          break;
        case SDL_MOUSEMOTION:
          mousePosition.x = event.motion.x;
          mousePosition.y = event.motion.y;
          break;
        }
    }
}

