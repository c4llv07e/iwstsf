
#ifndef IWS_TEXTURE_INC
#define IWS_TEXTURE_INC

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

SDL_Texture* textureLoad(SDL_Renderer* renderer, const char* path);

#endif
