
#ifndef IWS_DELTA_TIME_INC
#define IWS_DELTA_TIME_INC

extern double deltaTime;

void deltaBeforeFrame(void);
void deltaAfterFrame(void);

#endif
