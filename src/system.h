
#ifndef IWN_SYSTEM_INC
#define IWN_SYSTEM_INC

#include <SDL2/SDL.h>

extern SDL_Window* window;
extern SDL_Renderer* renderer;

void systemInit(void);
void systemDeinit(void);
_Noreturn void systemExit(void);

#endif
