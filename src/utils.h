
#ifndef IWS_UTILS_INC
#define IWS_UTILS_INC

#include <SDL2/SDL.h>

void setColor(SDL_Renderer* renderer, SDL_Color color);
SDL_Rect frectToRect(SDL_FRect frect);
SDL_bool fpointInFrect(SDL_FPoint point, SDL_FRect rect);
SDL_bool frectInFrect(SDL_FRect a, SDL_FRect b);
SDL_bool frayInFrect(SDL_FPoint start, SDL_FPoint vel, SDL_FRect rect,
                     SDL_FPoint* contactPoint, SDL_FPoint* contactNormal,
                     double* nearHit);
SDL_FRect expandFrect(SDL_FRect rect, SDL_FRect origin);
SDL_bool frectMovefrect(SDL_FRect rect, SDL_FPoint vel, SDL_FRect wall,
                        SDL_FPoint *contactPoint, SDL_FPoint *contactNormal,
                        double *nearHit, float delta);
SDL_FPoint moveFrect(SDL_FRect rect, SDL_FPoint* vel, SDL_FRect* walls, Uint32 wallsCount);

#endif
