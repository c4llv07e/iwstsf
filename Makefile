BIN_NAME := iwstsf
CC = clang
SRC_POSTFIX := c
HEADER_POSTFIX := h
SRC_PATH := src
BUILD_PATH := _build
LIBS = sdl2 SDL2_image
SANITIZER = -fsanitize=address,leak,undefined,pointer-compare,pointer-subtract,shadow-call-stack
FLAGS = -Wall -Wpedantic -std=c17
CCFLAGS = ${FLAGS}
LDFLAGS = ${FLAGS}

rwildcard = $(foreach d, $(wildcard $1*), $(call rwildcard,$d/,$2) \
						$(filter $(subst *,%,$2), $d))
CCFLAGS += `pkg-config --libs ${LIBS}`
LDFLAGS += `pkg-config --cflags ${LIBS}`
SOURCES := $(call rwildcard, $(SRC_PATH), *.$(SRC_POSTFIX))
OBJECTS = $(SOURCES:$(SRC_PATH)/%.$(SRC_POSTFIX)=$(BUILD_PATH)/%.o)

${BIN_NAME}: ${OBJECTS}
	${CC} ${CCFLAGS} ${OBJECTS} -o $@

${BUILD_PATH}/%.o: ${SRC_PATH}/%.${SRC_POSTFIX} ${SRC_PATH}/%.${HEADER_POSTFIX}
	mkdir -p $(@D) 
	${CC} ${LDFLAGS} -c $< -o $@

.PHONY: all build run clean dirs debug debugrun

all: build

build: ${BIN_NAME}

run: ${BIN_NAME}
	./$^

debug: CCFLAGS += -g -DDEBUG
debug: LDFLAGS += -g -DDEBUG

debugrun: CCFLAGS += -g -DDEBUG
debugrun: LDFLAGS += -g -DDEBUG

debug: ${BIN_NAME}
	gdb -tui $^

debugrun: ${BIN_NAME}
	./$^

clean:
	rm -rf ${BUILD_PATH}
