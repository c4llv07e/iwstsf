
#include "utils.h"
#include <math.h>

void
setColor(SDL_Renderer* renderer, SDL_Color color)
{
  SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
}

SDL_Rect
frectToRect(SDL_FRect frect)
{
  return (SDL_Rect) { .x = frect.x, .y = frect.y,
                      .w = frect.w, .h = frect.h };
}

SDL_bool
fpointInFrect(SDL_FPoint point, SDL_FRect rect)
{
  return (point.x >= rect.x && point.x <= rect.x + rect.w)
    && (point.y >= rect.y && point.y <= rect.y + rect.h);
}

SDL_bool
frectInFrect(SDL_FRect a, SDL_FRect b)
{
  return (a.x <= b.x + b.w && a.x + a.w >= b.x) &&
    (a.y <= b.y + b.h && a.y + a.h >= b.y);
}

SDL_bool
frayInFrect(SDL_FPoint start, SDL_FPoint vel, SDL_FRect rect,
            SDL_FPoint* contactPoint, SDL_FPoint* contactNormal, double* nearHit)
{
  SDL_FPoint tNear, tFar, _contactPoint, _contactNormal;
  double farHit, _nearHit;
  SDL_FPoint invdir;

  if (nearHit == NULL)
    nearHit = &_nearHit;
  if (contactPoint == NULL)
    contactPoint = &_contactPoint;
  if (contactNormal == NULL)
    contactNormal = &_contactNormal;

  invdir.x = 1.0f / vel.x;
  invdir.y = 1.0f / vel.y;
  
  tNear.x = (rect.x - start.x) * invdir.x;
  tNear.y = (rect.y - start.y) * invdir.y;
  tFar.x = (rect.x + rect.w - start.x) * invdir.x;
  tFar.y = (rect.y + rect.h - start.y) * invdir.y;

  if (isnan(tNear.x) || isnan(tNear.y)
      || isnan(tFar.x) || isnan(tFar.y))
    return SDL_FALSE;

  {
    double tmp;
    if (tNear.x > tFar.x)
      {
        tmp = tNear.x;
        tNear.x = tFar.x;
        tFar.x = tmp;
      } 
    if (tNear.y > tFar.y)
      {
        tmp = tNear.y;
        tNear.y = tFar.y;
        tFar.y = tmp;
      }
  }

  if (tNear.x > tFar.y || tNear.y > tFar.x)
    return SDL_FALSE;

  *nearHit = SDL_max(tNear.x, tNear.y);
  farHit = SDL_min(tFar.x, tFar.y);

  if (farHit < 0) return SDL_FALSE;

  contactPoint->x = start.x + *nearHit * vel.x;
  contactPoint->y = start.y + *nearHit * vel.y;

  if (tNear.x > tNear.y)
    {
      if (vel.x < 0)
        {
          contactNormal->x = 1;
          contactNormal->y = 0;
        }
      else
        {
          contactNormal->x = -1;
          contactNormal->y = 0;
        }
    }
  else
    {
      if (vel.y < 0)
        {
          contactNormal->x = 0;
          contactNormal->y = 1;
        }
      else
        {
          contactNormal->x = 0;
          contactNormal->y = -1;
        }
    }

  return SDL_TRUE;
}

SDL_FRect
expandFrect(SDL_FRect rect, SDL_FRect origin)
{
  SDL_FRect exp;

  exp.x = rect.x - (origin.w / 2.0f);
  exp.y = rect.y - (origin.h / 2.0f);
  exp.w = rect.w + origin.w;
  exp.h = rect.h + origin.h;

  return exp;
}

SDL_bool
frectMovefrect(SDL_FRect rect, SDL_FPoint vel, SDL_FRect wall,
               SDL_FPoint* contactPoint, SDL_FPoint* contactNormal, double* nearHit, float delta)
{
  SDL_FRect exp;
  SDL_FPoint center;
  SDL_FPoint deltaVel;
  double _nearHit;

  if (nearHit == NULL)
    nearHit = &_nearHit;
  
  if (vel.x == 0 && vel.y == 0)
    return SDL_FALSE;

  exp = expandFrect(wall, rect);

  center.x = rect.x + (rect.w / 2.0f);
  center.y = rect.y + (rect.h / 2.0f);

  deltaVel.x = vel.x * delta;
  deltaVel.y = vel.y * delta;

  return (frayInFrect(center, deltaVel, exp, contactPoint, contactNormal, nearHit) && *nearHit <= 1.0f);
}

SDL_FPoint
moveFrect(SDL_FRect rect, SDL_FPoint* vel, SDL_FRect* walls, Uint32 wallsCount)
{
  double length;
  SDL_FPoint cn;
  SDL_FPoint result;

  result = (SDL_FPoint){ 0.0f };
  
  for (Uint32 i = 0; i < wallsCount; ++i)
    {
      if (frectMovefrect(rect, *vel, walls[i], NULL, &cn, &length, 1.0f)
          && length >= 0.0f)
        {
          vel->x += cn.x * SDL_fabs(vel->x) * (1.0f - length);
          vel->y += cn.y * SDL_fabs(vel->y) * (1.0f - length);
          result.x += cn.x;
          result.y += cn.y;
        }
    }

  return result;
}
