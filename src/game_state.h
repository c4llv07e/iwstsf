
#ifndef IWS_GAME_STATE_INC
#define IWS_GAME_STATE_INC

#include <SDL2/SDL.h>
#include "scene.h"
#include "arena.h"

typedef struct Player Player;
struct Player
{
  SDL_FRect rect;
  SDL_FPoint vel;
  double speed;
  SDL_bool isOnFloor;
  double jumpHeight;
  Uint8 jumpsDone;
  SDL_bool lastJump;
};

typedef struct GameState GameState;
struct GameState
{
  SDL_bool isGameRunning;
  Uint32 wallsCount;
  SDL_FRect* walls;
  Player* player;
  Scene* scene;
  Arena sceneRectsArena;
  SDL_FRect* sceneRects;
  double gravity;
};

GameState* allocGameState(void);
void freeGameState(GameState* gameState);

#endif
