
#ifndef IWS_EVENTS_INC
#define IWS_EVENTS_INC

#include "game_state.h"
#include <SDL2/SDL.h>

extern SDL_bool keysDown[SDL_NUM_SCANCODES];
extern SDL_Point mousePosition;

void pollEvents(GameState* state);

#endif
