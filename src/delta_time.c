
#include "delta_time.h"
#include <SDL2/SDL.h>

double deltaTime = 1.0f;
Uint64 currentFrame = 0;
Uint64 lastFrame = 0;

void
deltaBeforeFrame(void)
{
  currentFrame = SDL_GetPerformanceCounter();
  deltaTime = (double)((currentFrame - lastFrame) * 1000 / (double)SDL_GetPerformanceFrequency());
}

void
deltaAfterFrame(void)
{
  /* fix big delta at the start */
  lastFrame = (currentFrame!=0)?currentFrame:SDL_GetPerformanceCounter();
}
